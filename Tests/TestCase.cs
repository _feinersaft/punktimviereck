﻿using PunktImViereck;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tests
{
    public class TestCase
    {
        public Square TestSquare;
        public Point SearchingPoint;
        public bool ExpectedOutput;

        public TestCase(Square s, Point p, bool b)
        {
            TestSquare = s;
            SearchingPoint = p;
            ExpectedOutput = b;
        }

        public static List<TestCase> GetTestCases()
        {
            return new List<TestCase>
            {
                new TestCase(
                    new Square(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10)),
                    new Point(20, 20),
                    false),
                new TestCase(
                    new Square(new Point(0, 0), new Point(10, 0), new Point(10, 10), new Point(0, 10)),
                    new Point(5, 5),
                    true),
                new TestCase(
                    new Square(new Point(0, 0), new Point(0, 5), new Point(5, 0), new Point(5, 5)),
                    new Point(6, 3),
                    false),
                new TestCase(
                    new Square(new Point(0, 0), new Point(0, 5), new Point(5, 0), new Point(5, 5)),
                    new Point(3, 3),
                    true),
                new TestCase(
                    new Square(new Point(0, 0), new Point(0, 5), new Point(5, 0), new Point(5, 5)),
                    new Point(5, 5),
                    true),
                new TestCase(
                    new Square(new Point(0, 0), new Point(0, 5), new Point(5, 0), new Point(5, 5)),
                    new Point(0, 2),
                    true)
            };
        }
    }
}