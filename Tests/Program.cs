﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PunktImViereck;

namespace Tests
{
    class Program
    {
        static void Main(string[] args)
        {
            List<TestCase> testCases = TestCase.GetTestCases();
            TestCase testCase;

            for (int i = 0; i < testCases.Count; i++)
            {
                testCase = testCases[i];

                Console.WriteLine("Testfall Nr." + (i + 1));
                if (testCase.TestSquare.IsPointInSquare(testCase.SearchingPoint) == testCase.ExpectedOutput)
                {
                    Console.WriteLine("Algorithmus 1 Test erfolgreich");
                }
                else
                {
                    Console.WriteLine("Algorithmus 1 Test NICHT erfolgreich");
                }

                if (testCase.TestSquare.IsPointInSquare2(testCase.SearchingPoint) == testCase.ExpectedOutput)
                {
                    Console.WriteLine("Algorithmus 2 Test erfolgreich");
                }
                else
                {
                    Console.WriteLine("Algorithmus 2 Test NICHT erfolgreich");
                }

                Console.WriteLine();
            }

            Console.ReadKey();
            Console.ReadKey();
        }
    }
}