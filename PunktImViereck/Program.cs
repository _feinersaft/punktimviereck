﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PunktImViereck
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Point> points = new List<Point>();
            Square square;
            int algo;
            bool output = false;
            bool output2 = false;

            Console.WriteLine("Für alle Punkte nur Ganze Zahlen verwenden!");

            // Iterate through ASCII codes from A to (including) E
            for (int i = 'A'; i <= 'E'; i++)
            {
                char currentPoint = (char) i;
                int coordinateX;
                int coordinateY;

                Console.WriteLine("Bitte Koordinaten für Punkt " + currentPoint + " eingeben:");

                Console.Write("Koordinate X: ");
                // If an uncovertable data is given, repeat the prompt
                while (!int.TryParse(Console.ReadLine(), out coordinateX))
                {
                    Console.WriteLine("Bitte gültige Zahl eingeben!");
                }

                Console.Write("Koordinate Y: ");
                while (!int.TryParse(Console.ReadLine(), out coordinateY))
                {
                    Console.WriteLine("Bitte gültige Zahl eingeben!");
                }

                Point point = new Point(coordinateX, coordinateY);

                if (PointExists(points, point))
                {
                    i--;
                    Console.WriteLine("Punkt existiert bereits! Bitte neuen Punkt eingeben.");
                }

                points.Add(point);
                Console.WriteLine();
            }

            square = new Square(points[0], points[1], points[2], points[3]);

            Console.Clear();


            Console.WriteLine("Welchen Algorithmus möchten Sie anwenden?");
            Console.WriteLine("(1) Algorithmus 'Dreieck-Methode'");
            Console.WriteLine("(2) Algorithmus 'Raubkopierte-Methode-ohne-Namen'");
            Console.WriteLine("(3) beide Algorithmen");
            // Repeat the prompt, when the entered number is not 1 or 2
            while (!int.TryParse(Console.ReadLine(), out algo) || !(algo == 1 || algo == 2 || algo == 3))
            {
                Console.WriteLine("Bitte gültige Zahl eingeben!");
            }

            switch (algo)
            {
                case 1:
                    output = square.IsPointInSquare(points[points.Count - 1]);
                    break;
                case 2:
                    output = square.IsPointInSquare2(points[points.Count - 1]);
                    break;
                case 3:
                    output = square.IsPointInSquare(points[points.Count - 1]);
                    output2 = square.IsPointInSquare2(points[points.Count - 1]);
                    break;
            }

            if (algo == 3)
            {
                if (output)
                {
                    Console.WriteLine("Laut Algorithmus 1 liegt der Punkt im Rechteck!");
                }
                else
                {
                    Console.WriteLine("Laut Algorithmus 1 liegt der Punkt NICHT im Rechteck!");
                }

                if (output2)
                {
                    Console.WriteLine("Laut Algorithmus 2 liegt der Punkt im Rechteck!");
                }
                else
                {
                    Console.WriteLine("Laut Algorithmus 2 liegt der Punkt NICHT im Rechteck!");
                }
            }
            else if (output)
            {
                Console.WriteLine("Der Punkt liegt im Rechteck!");
            }
            else
            {
                Console.WriteLine("Der Punkt liegt NICHT im Rechteck!");
            }

            Console.ReadKey();
        }

        private static bool PointExists(List<Point> points, Point point)
        {
            foreach (Point item in points)
            {
                if (item.X == point.X && item.Y == point.Y)
                {
                    return true;
                }
            }

            return false;
        }
    }
}