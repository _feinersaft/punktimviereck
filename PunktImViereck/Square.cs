﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PunktImViereck
{
    public class Square
    {
        public Point A;
        public Point B;
        public Point C;
        public Point D;

        public Square()
        {
        }

        public Square(Point a, Point b, Point c, Point d)
        {
            A = a;
            B = b;
            C = c;
            D = d;
        }

        public bool IsPointInSquare(Point p)
        {
            try
            {
                return BarycentricMethod(A, B, D, p) || BarycentricMethod(B, C, D, p);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public bool IsPointInSquare2(Point p)
        {
            Point[] points = new Point[] {A, B, C, D};

            try
            {
                return SecondAlgorithm.IsInside(points, points.Length, p);
            }
            catch (Exception)
            {
                return false;
            }
        }

        private bool BarycentricMethod(Point p1, Point p2, Point p3, Point p)
        {
            int denom = ((p2.Y - p3.Y) * (p1.X - p3.X) + (p3.X - p2.X) * (p1.Y - p3.Y));
            int nom_a = ((p2.Y - p3.Y) * (p.X - p3.X) + (p3.X - p2.X) * (p.Y - p3.Y));
            int nom_b = ((p3.Y - p1.Y) * (p.X - p3.X) + (p1.X - p3.X) * (p.Y - p3.Y));
            int a = nom_a / denom;
            int b = nom_b / denom;
            int c = 1 - a - b;
            return ((0 <= a && a <= 1) && (0 <= b && b <= 1) && (0 <= c && c <= 1));
        }

        private static class SecondAlgorithm
        {
            // Given three colinear points p, q, r, the function checks if 
            // point q lies on line segment 'pr' 
            static bool OnSegment(Point p, Point q, Point r)
            {
                if (q.X <= Math.Max(p.X, r.X) && q.X >= Math.Min(p.X, r.X) &&
                    q.Y <= Math.Max(p.Y, r.Y) && q.Y >= Math.Min(p.Y, r.Y))
                    return true;
                return false;
            }

            // To find orientation of ordered triplet (p, q, r). 
            // The function returns following values 
            // 0 --> p, q and r are colinear 
            // 1 --> Clockwise 
            // 2 --> Counterclockwise 
            static int Orientation(Point p, Point q, Point r)
            {
                int val = (q.Y - p.Y) * (r.X - q.X) -
                          (q.X - p.X) * (r.Y - q.Y);

                if (val == 0) return 0; // colinear 
                return (val > 0) ? 1 : 2; // clock or counterclock wise 
            }

            // The function that returns true if line segment 'p1q1' 
            // and 'p2q2' intersect. 
            static bool DoIntersect(Point p1, Point q1, Point p2, Point q2)
            {
                // Find the four orientations needed for general and 
                // special cases 
                int o1 = Orientation(p1, q1, p2);
                int o2 = Orientation(p1, q1, q2);
                int o3 = Orientation(p2, q2, p1);
                int o4 = Orientation(p2, q2, q1);

                // General case 
                if (o1 != o2 && o3 != o4)
                    return true;

                // Special Cases 
                // p1, q1 and p2 are colinear and p2 lies on segment p1q1 
                if (o1 == 0 && OnSegment(p1, p2, q1)) return true;

                // p1, q1 and p2 are colinear and q2 lies on segment p1q1 
                if (o2 == 0 && OnSegment(p1, q2, q1)) return true;

                // p2, q2 and p1 are colinear and p1 lies on segment p2q2 
                if (o3 == 0 && OnSegment(p2, p1, q2)) return true;

                // p2, q2 and q1 are colinear and q1 lies on segment p2q2 
                if (o4 == 0 && OnSegment(p2, q1, q2)) return true;

                return false; // Doesn't fall in any of the above cases 
            }

            // Returns true if the point p lies inside the polygon[] with n vertices 
            public static bool IsInside(Point[] polygon, int n, Point p)
            {
                // There must be at least 3 vertices in polygon[] 
                if (n < 3) return false;

                // Create a point for line segment from p to infinite 
                Point extreme = new Point(int.MaxValue, p.Y);

                // Count intersections of the above line with sides of polygon 
                int count = 0, i = 0;
                do
                {
                    int next = (i + 1) % n;

                    // Check if the line segment from 'p' to 'extreme' intersects 
                    // with the line segment from 'polygon[i]' to 'polygon[next]' 
                    if (DoIntersect(polygon[i], polygon[next], p, extreme))
                    {
                        // If the point 'p' is colinear with line segment 'i-next', 
                        // then check if it lies on segment. If it lies, return true, 
                        // otherwise false 
                        if (Orientation(polygon[i], p, polygon[next]) == 0)
                            return OnSegment(polygon[i], p, polygon[next]);

                        count++;
                    }

                    i = next;
                } while (i != 0);

                // Return true if count is odd, false otherwise 
                return count % 2 == 1;
            }
        }
    }
}